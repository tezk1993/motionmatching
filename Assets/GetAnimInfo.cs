﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
public class GetAnimInfo : MonoBehaviour {

    [Serializable]
    public struct BoneState
    {
        public HumanBodyBones boneId;
        public Quaternion angle;
        public Quaternion velocity;
        public Quaternion nextVelocity;
    }

    [Serializable]
    public struct MotionState
    {
        public List<BoneState> skeletonState;
        public float hipYpos;

        public Vector3 hipGlobalPosition;
        public Vector3 hipGlobalVelocity;
        public Vector3 hipNextGlobalVelocity;


        public Quaternion hipAngle;
        public Quaternion hipAngularVelocity;
        public Quaternion hipNextAngularVelocity;

        public object Clone()
        {
            MotionState clone = this;
            // Make a clone of the skeleton state
            clone.skeletonState = this.skeletonState.Select(x => x).ToList();
            return clone;
        }
    }

    [Serializable]
    public struct MotionClip
    {
        [HideInInspector]
        public string animName;
        public List<MotionState> frames;
    }

    public List<MotionClip> animList;
    public List<string> animatorStates;

    Animator anim;
    AnimationState state;
    [Range(0, 1)]
    public float animFrame = 0;
    
    public Transform rootTransform;
    public Vector3 previousPos;
    public Vector3 currentPos;
    private bool createGizmos;

    public HumanBodyBones[] allBones;


    // Use this for initialization

    void Awake () {
        anim = gameObject.GetComponent<Animator>();
        allBones = (HumanBodyBones[])Enum.GetValues(typeof(HumanBodyBones));



    }

    // Update is called once per frame
    void Update () {
        //Debug.Log(currentClip.frameRate * currentClip.length);
        //anim.Play("9Step_Walk_1", -1, animFrame);
        //DrawTracjectory();
    }

    [ContextMenu("Create motion clips")]
    void establishMotionSynth(){


      
        for (int i = 0; i < animatorStates.Capacity; i++)
        {
            var currClip = new MotionClip { frames = new List<MotionState>() };
            currClip.animName = animatorStates[i];
            animList.Add(currClip);
            gameObject.transform.position = Vector3.zero;

            createMotionState(animatorStates[i], i);

        }
        for (int i = 0; i < animList.Count; i++)
        {
          DrawTracjectoryOfCurrentPosList(animList[i]);
        }
    }

    void createMotionState(string stateName, int animStateindex)
    {
        const int startFrame = 1;

        anim.Play(stateName, -1, 0);
        anim.Update(1);
        
        var clip = anim.GetCurrentAnimatorClipInfo(0)[0].clip;
        //clipsFramerate = (int)clip.frameRate;

        // Subtracting one because sometimes the last frame wraps around to the first frame.
        int clipFrameCount = Mathf.Max(0, (int)(clip.length * clip.frameRate) - 1);


        anim.speed = 0.0001f;
        anim.Play(stateName, -1, (startFrame / clip.frameRate) / clip.length);
        anim.Update(1);

        for (int frameId = startFrame; frameId < clipFrameCount; frameId++)
        {
            var currMotionState = new MotionState();
            var hipTransform = anim.GetBoneTransform(HumanBodyBones.Hips);
            currMotionState.skeletonState = new List<BoneState>();
            currMotionState.hipGlobalPosition = hipTransform.position;
           // currMotionState.hipGlobalPosition.y = Mathf.Max(currMotionState.hipGlobalPosition.y, minHipY);
            currMotionState.hipGlobalPosition.x = 0;
            currMotionState.hipAngle = hipTransform.rotation;
            var euler = hipTransform.rotation.eulerAngles;
            for (int i = 0; i < allBones.Length; i++)
            {
                if (allBones[i] != HumanBodyBones.LastBone)
                {
                    var transform = anim.GetBoneTransform(allBones[i]);
                    if (transform != null)
                    {
                        BoneState bone = new BoneState();
                        bone.angle = transform.localRotation;
                        bone.boneId = allBones[i];
                        currMotionState.skeletonState.Add(bone);
                    }
                }
            }
            animList[animStateindex].frames.Add(currMotionState);
            float normalizedTime = (frameId / clip.frameRate) / clip.length;

            anim.Play(stateName, -1, normalizedTime);
             
            anim.Update(1);

            
        }

        
    }

    [ContextMenu("Create Circle Path")]
    void DrawSpecificTrajectory()
    {
        for (int i = 0; i < animList[4].frames.Count; i++)
        {
            Debug.Log(i + " " + animList[4].frames.Count);
            if (i > 0)
                Debug.DrawLine(animList[4].frames[i - 1].hipGlobalPosition, animList[4].frames[i].hipGlobalPosition, Color.red, 200f);
        }

    }

    void DrawTracjectoryOfCurrentPosList(MotionClip currMotionClip)
    {
        for (int i = 0; i < currMotionClip.frames.Count; i++)
        {
            Debug.Log(i + " " + currMotionClip.frames.Count);
            if(i > 0)
                Debug.DrawLine(currMotionClip.frames[i-1].hipGlobalPosition, currMotionClip.frames[i].hipGlobalPosition, Color.red, 200f);
        }
        
    }


    [ContextMenu("Create path points")]
    void createPathPoints()
    {
        createGizmos = true;
    }

    void OnDrawGizmos()
    {
        if (createGizmos == true)
        {
            for (int i = 0; i < animList.Count; i++)
            {
                for (int j = 0; j < animList[i].frames.Count; j++)
                {
                    Gizmos.DrawSphere(animList[i].frames[j].hipGlobalPosition, 1);

                }
            }
        }
    }
}
