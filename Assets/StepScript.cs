﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepScript : MonoBehaviour {

    public GameObject Player;

    MotionMatching motionMatching;

    Animator anim;

    public Vector3 leftFootpos;
    public Vector3 rightFootpos;

    public GameObject waterRipple;
    public GameObject spriteParticles;

    private void Start()
    {

        motionMatching = Player.GetComponent<MotionMatching>();
        anim = gameObject.GetComponent<Animator>();
    }


    void StepLeft()
    {
        if (anim == motionMatching.currPlaybackAnimator)
        {
            GameObject waterRippleLeft = GameObject.Instantiate(waterRipple, leftFootpos,waterRipple.transform.rotation);
            GameObject spriteParticlesLeft = GameObject.Instantiate(spriteParticles, leftFootpos, waterRipple.transform.rotation);

        }
    }

    void StepRight()
    {
        if(anim == motionMatching.currPlaybackAnimator) {
            GameObject waterRippleRight = GameObject.Instantiate(waterRipple, rightFootpos, waterRipple.transform.rotation);
            GameObject spriteParticlesRight = GameObject.Instantiate(spriteParticles, rightFootpos, waterRipple.transform.rotation);

        }
    }
}
