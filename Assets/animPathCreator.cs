﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
public class animPathCreator : MonoBehaviour {

    Animator anim;

    [Serializable]
    public struct MotionState
    {
        public Vector3 pathPoint;
    }

    [Serializable]
    public struct MotionClip
    {
        [HideInInspector]
        public string animName;
        public List<MotionState> frames;
    }
    [Range(0, 1)]
    public float frameNumber;

    [SerializeField]
    public enum animStates // your custom enumeration
    {
        NineStep_Jog_One,
        NineStep_Walk_One,
        BackAndForth_Jog_One,
        BackAndForth_Walk,
        Circle_Clockwise_Jog,
        Circle_Clockwise_Walk,
        Snake_Jog,
        Snake_Walk,
        Snake_Clockwise_Turns,
        Snake_CounterClockwise_Turns,
        Circle_CounterClockwise_Jog,
        Circle_CounterClockwise_Walk_One,
        Giant_Sword_Swing
    };
    public animStates animState  = animStates.NineStep_Jog_One;

    public List<MotionClip> animList;

    // Use this for initialization
    void Start () {
        anim = gameObject.GetComponent<Animator>();
        anim.speed = 0;
        
    }

    [ContextMenu("Get Selected Anim Path")]
    private void getAnimPath()
    {
        AnimationClip clip = anim.GetCurrentAnimatorClipInfo(0)[0].clip;
        var currMotionClip = new MotionClip();
        int clipFrameCount = Mathf.Max(0, (int)(clip.length * clip.frameRate) - 1);


        currMotionClip.animName = animState.ToString();
        currMotionClip.frames = new List<MotionState>();
        animList.Add(currMotionClip);


        const int startFrame = 1;

        anim.Play(animState.ToString(), -1, 0);
        anim.Update(1);

        //clipsFramerate = (int)clip.frameRate;

        // Subtracting one because sometimes the last frame wraps around to the first frame.

        anim.speed = 0.0001f;
        anim.Play(animState.ToString(), -1, (startFrame / clip.frameRate) / clip.length);
        anim.Update(1);

        for (int frameId = startFrame; frameId < clipFrameCount; frameId++)
        {
            var currMotionState = new MotionState();

            currMotionState.pathPoint = gameObject.transform.position;

            animList[animState.GetHashCode()].frames.Add(currMotionState);

            float normalizedTime = (frameId / clip.frameRate) / clip.length;

            anim.Play(animState.ToString(), -1, normalizedTime);

            anim.Update(1);
        }

       
    }

    private void Update()
    {
        playAnimation();
    }

    [ContextMenu("Play Selected Anim")]
    void playAnimation()
    {

        gameObject.transform.position = Vector3.zero;
        AnimationClip clip = anim.GetCurrentAnimatorClipInfo(0)[0].clip;
        string selectedAnim = animState.ToString();
        Debug.Log(Mathf.FloorToInt((frameNumber * clip.frameRate) * clip.length));
        anim.Play(selectedAnim,-1, frameNumber);
        anim.Update(1);
    }
}
