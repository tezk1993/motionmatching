﻿/**
 * MotionMatchingDatabase.cs
 * Created by: Artur Barnabas
 * Created on: 15/10/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class Vector3Surrogate : ISerializationSurrogate
{
    /// <summary>
    /// Manually add objects to the <see cref="SerializationInfo"/> store.
    /// </summary>
    public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
    {
        Vector3 vector = (Vector3) obj;
        info.AddValue("x", vector.x);
        info.AddValue("y", vector.y);
        info.AddValue("z", vector.z);
    }

    /// <summary>
    /// Retrieves objects from the <see cref="SerializationInfo"/> store.
    /// </summary>
    /// <returns></returns>
    public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
    {
        Vector3 vector = (Vector3) obj;
        vector.x = info.GetSingle("x");
        vector.y = info.GetSingle("y");
        vector.z = info.GetSingle("z");
        return vector;
    }
}

/// --------------------------------------------------------------------------------------
/// Contains some information about a single pose and associated movement trajectory.
/// Among these are the position and velocity of the feet relative to the body position as
/// well as the past and future positions of the body.
///
///
/// This information is used during runtime to determine which should be the next pose given the
/// current pose and a desired trajectory.
/// --------------------------------------------------------------------------------------
[Serializable]
public struct MotionState
{
    /// The following few variables describe the position of some specific parts of the body in
    /// a coordinate system that is relative to the current pose. More specifically a coordinate
    /// system that originates from the point below the hip on the floor (Y=0 plane)
    /// and has the same orientation around the y axis as the hip. That is the XZ plane (floor) is
    /// parallel to the global XZ plane but rotated around the Y axis
    public float hipHeight;
    public Vector3 leftFootPos;
    public Vector3 leftFootVelocity;
    public Vector3 rightFootPos;
    public Vector3 rightFootVelocity;

    /// The following arrays contain a few points from the trajectory equal time from each other in
    /// the past and in the future. All these points are relative to the current pose (see above)
    /// and are on the floor below the hip's then position.
    public Vector3[] pastPositions;
    public Vector3[] futurePositions;
}

[Serializable]
public struct FrameId : IEquatable<FrameId>
{
    public string stateName;
    public int frameNumber;

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        
        return Equals((FrameId)obj);
    }

    public bool Equals(FrameId other)
    {
        return (stateName == other.stateName) && (frameNumber == other.frameNumber);
    }
    
    public override int GetHashCode()
    {
        return frameNumber;
    }

    public override string ToString()
    {
        return string.Format("{0}, {1}", stateName, frameNumber);
    }
}

/// --------------------------------------------------------------------------------------
/// The database class itself.
/// This class is responsible for preprocessing the recorded animations in order to generate
/// a database that will be read during runtime to help deciding what should be the next pose
/// at a given state. This class also stores all that data generated during the preprocessing.
/// --------------------------------------------------------------------------------------
public class MotionMatchingDatabase : MonoBehaviour
{
    #region Types
    /// A pose descriptor that is stored in the database.
    /// This struct also stores the nearest neighbours of the pose wheras the
    /// pose descriptor struct may be used runtime to describe the current state.
    [Serializable]
    public class DatabaseMotionState
    {
        public MotionState state;

        public FrameId[] nearestNeighbours;
    }

    [Serializable]
    class PersistentDatabase
    {
        public Dictionary<FrameId, DatabaseMotionState> frames;
        public Dictionary<string, float> stateLength;
    }

    struct MotionStateBuildInfo
    {
        public Matrix4x4 worldToBody;
        public Vector3 bodyWorldPos;
    }

    struct SortableFrames
    {
        public FrameId frameId;
        public float distance;
    }

    struct MotionStatePairDistance
    {
        public FrameId frameIdA;
        public FrameId frameIdB;
        public float distance;
    }
    #endregion

    #region PublicVariables
    public const int TRAJECTORY_POS_FREQUENCY = 10;
    public const float TRAJECTORY_SECONDS = 1.5f;
    public const int TRAJECTORY_POS_COUNT = (int)(TRAJECTORY_POS_FREQUENCY * TRAJECTORY_SECONDS);
    public const int TRAJECTORY_FRAME_STEP = (int)(TRAJECTORY_SECONDS * SAMPLE_FRAMERATE) / TRAJECTORY_POS_COUNT;

    public const float POSE_VS_TRAJ_BUILD_TIME = 0.5f;

    public int idleFrameCount;
    public int attackFrameCount;

    #endregion

    #region PublicProperties
    public Dictionary<FrameId, DatabaseMotionState> frames { get {return persistent.frames; } }
    public bool rebuildDatabase = false;
    #endregion

    #region PrivateVariables
    private const int SAMPLE_FRAMERATE = 14;
    private const float SAMPLE_DELTA_TIME = 1 / (float)SAMPLE_FRAMERATE;
    private const int NEAREST_NEIGHBOUR_COUNT = 200;

    // what percentage of datapoints should be filtered from the source set
    // which are the closest to other points
    private const float DUPLICATE_REMOVAL_RATIO = 0.3f;

    private string databaseFileStem = "MotionMatchingDatabase";
    private string databaseFileExtension = ".bytes";

    private string databaseFilePath { get { return "Assets\\Resources\\" + databaseFileStem + databaseFileExtension; } }

    private BinaryFormatter databaseFormatter;

    private PersistentDatabase persistent;

    private Animator animator;

    #endregion

    #region Unity Methods
    public void Awake()
    {
        persistent = new PersistentDatabase();
        persistent.frames = new Dictionary<FrameId, DatabaseMotionState>();
        persistent.stateLength = new Dictionary<string, float>();

        SetUpDatabaseFormatter();

        if (!rebuildDatabase) ReadDatabaseFromFile();
    }
    #endregion


    #region PublicMethods
    public void RebuildDatabase(Animator animator)
    {
        this.animator = animator;

        AddAnimationToDatabase("mc-9step-walk-part1");
        AddAnimationToDatabase("mc-9step-walk-part2");
        AddAnimationToDatabase("mc-circle-cw-walk");
        AddAnimationToDatabase("mc-circle-ccw-walk");
        AddAnimationToDatabase("Idle");
        AddAnimationToDatabase("Attack");

        FilterCloseMotionStates();
        CalculateNearestNeighbours();

        WriteDatabaseToFile();
    }

    public string[] GetStateNames() {
        return persistent.stateLength.Keys.ToArray();
    }

    public void PlayClipFromFrame(Animator animator, FrameId frameId)
    {
        float normalizedTime = FrameIdToNormalizedTime(frameId);
        animator.Play(frameId.stateName, -1, normalizedTime);
    }

    public float FrameIdToNormalizedTime(FrameId frameId)
    {
        float length = persistent.stateLength[frameId.stateName];
        return ((float)frameId.frameNumber / SAMPLE_FRAMERATE) / length;
    }

    public static Vector3 GetBodyPositionFromHip(Vector3 hipPos)
    {
        return new Vector3(hipPos.x, 0, hipPos.z);
    }

    public static void DecomposeTRS(Matrix4x4 m, out Vector3 translation, out Quaternion rotation, out Vector3 scale)
    {
        translation = m.GetColumn(3);

        // Extract new local rotation
        rotation = Quaternion.LookRotation(
            m.GetColumn(2),
            m.GetColumn(1)
        );
        
        // Extract new local scale
        scale = new Vector3(
            m.GetColumn(0).magnitude,
            m.GetColumn(1).magnitude,
            m.GetColumn(2).magnitude
        );
    }

    public static Matrix4x4 GetWorldToBodyTransform(Animator animator)
    {
        //var transform = animator.GetBoneTransform(HumanBodyBones.Hips);
        var transform = animator.transform;
        Quaternion rotation = transform.rotation;
        Vector3 scale = transform.lossyScale;
        Vector3 position = transform.position;

        var rotEuler = rotation.eulerAngles;

        // Only allow rotation around y
        rotEuler.x = 0;
        rotEuler.z = 0;

        // It is on the floor
        position.y = 0;

        var modifiedBodyToWorld = Matrix4x4.TRS(position, Quaternion.Euler(rotEuler), scale);
        return modifiedBodyToWorld.inverse;
    }

    public static Vector3 GetPositionRelativeTo(Matrix4x4 worldToLocal, Vector3 targetWorldPos)
    {
        // Inverse of the 'world to local' is 'local to world'. Transforming the zero vector
        // gives us the offset from 'local' to 'world'. Multiplying this offset by negative one
        // gives the inverse of said offset.
        //var targetInWorld = -1 * worldToTarget.MultiplyPoint(Vector3.zero);
        var targetInOrigin = worldToLocal.MultiplyPoint(targetWorldPos);
        return targetInOrigin;
    }

    public static float MotionStateDistance(MotionState stateA, MotionState stateB, float poseVsTrajectory)
    {
        float poseDistance = PoseDistace(stateA, stateB);

        float trajectoryDistance = 0;
        for (int i = 0; i < stateA.pastPositions.Length; i++)
        {
            var positionA = stateA.pastPositions[i];
            var positionB = stateB.pastPositions[i];
            // Past trajectory points are weighted equally
            trajectoryDistance += PastTrajectoryWeight(i) * (positionA - positionB).magnitude;
        }

        for (int i = 0; i < stateA.futurePositions.Length; i++)
        {
            var positionA = stateA.futurePositions[i];
            var positionB = stateB.futurePositions[i];
            trajectoryDistance += FutureTrajectoryWeight(i) * (positionA - positionB).magnitude;
        }

        if (poseDistance < 0 || poseDistance > 1000)
        {
            Debug.LogWarning(string.Format("Distance was {0}, which is not okay", poseDistance));
        }

        return Mathf.Lerp(poseDistance, trajectoryDistance, poseVsTrajectory);
    }
    #endregion

    #region PrivateMethods

    private void SetUpDatabaseFormatter()
    {
        var surrogateSelector = new SurrogateSelector();
        surrogateSelector.AddSurrogate(
			typeof (Vector3),
			new StreamingContext(StreamingContextStates.All),
            new Vector3Surrogate()
		);

		databaseFormatter = new BinaryFormatter();
		databaseFormatter.SurrogateSelector = surrogateSelector;
    }

    private void WriteDatabaseToFile()
    {
        Debug.Log(string.Format("Writing database to file..."));
		//Write backup copy first
		DateTime now = DateTime.Now;
		string backupFilename = string.Format(
			"{0}-{1}-{2}-{3}T{4}-{5}-{6}",
			databaseFileStem,
			now.Year,
			now.Month,
			now.Day,
			now.Hour,
			now.Minute,
            now.Second
		);
		using (var stream = new FileStream(backupFilename, FileMode.Create, FileAccess.Write)) {
			databaseFormatter.Serialize(stream, persistent);
		}

		// Overwrite database file
		System.IO.File.Copy(backupFilename, databaseFilePath, true);
    }

    private void ReadDatabaseFromFile()
    {
        var database = (TextAsset)Resources.Load(databaseFileStem);
		using (var stream = new MemoryStream(database.bytes)) {
			persistent = (PersistentDatabase)databaseFormatter.Deserialize(stream);
		}
    }

    void AddAnimationToDatabase(string stateName)
    {
		Debug.Log(string.Format("Adding motion states to database"));
		// Always skip the first frame (frame 0) because it is a T pose which is not part of the
		// recorded movements.
		const int startFrame = 1;

        animator.speed = 0;
        animator.applyRootMotion = true;
		animator.Play(stateName, -1, 0);
		animator.Update(1);
		var clip = animator.GetCurrentAnimatorClipInfo(0)[0].clip;

        persistent.stateLength.Add(stateName, clip.length);

		// Subtracting one because sometimes the last frame wraps around to the first frame.
		int clipFrameCount = Math.Max(0, (int)(clip.length * SAMPLE_FRAMERATE) - 1);

		Debug.Log(string.Format("{0} framecount: {1}", stateName, clipFrameCount));

        var currClip = new List<DatabaseMotionState>();
        var currClipBuildInfo = new List<MotionStateBuildInfo>();

        if (stateName == "Idle")
        {
            idleFrameCount = clipFrameCount;
        }
        else if (stateName == "Attack")
        {
            attackFrameCount = clipFrameCount;
        }
        animator.speed = 1;
        animator.applyRootMotion = true;
		for (int frameId = startFrame; frameId < clipFrameCount; frameId++)
        {
            float normalizedTime = frameId / (float)clipFrameCount;
            //animator.Play(stateName, -1, normalizedTime);
			animator.Update(SAMPLE_DELTA_TIME);

			var currState = new DatabaseMotionState();
            var currStateBuildInfo = new MotionStateBuildInfo();

			var hipTransform = animator.GetBoneTransform(HumanBodyBones.Hips);
			currState.state.hipHeight = hipTransform.position.y;
            var worldToBody = GetWorldToBodyTransform(animator);
            currStateBuildInfo.worldToBody = worldToBody;
            currStateBuildInfo.bodyWorldPos = hipTransform.position;
            currStateBuildInfo.bodyWorldPos.y = 0;
            
            var leftFootWorld = animator.GetBoneTransform(HumanBodyBones.LeftFoot).position;
            var rightFootWorld = animator.GetBoneTransform(HumanBodyBones.RightFoot).position;
            currState.state.leftFootPos = worldToBody.MultiplyPoint(leftFootWorld);
            currState.state.rightFootPos = worldToBody.MultiplyPoint(rightFootWorld);
			
            currClip.Add(currState);
            currClipBuildInfo.Add(currStateBuildInfo);
		}
	
		// Calculate trajectories and velocities
        const int trajectoryFrameLength = TRAJECTORY_POS_COUNT * TRAJECTORY_FRAME_STEP;
		for (int frameId = 0; frameId < currClip.Count; frameId++)
        {
            var currState = currClip[frameId].state;
            var worldToBody = currClipBuildInfo[frameId].worldToBody;

            // Past trajectory
            // Only add trajectory if there are enough frames to add
            if (frameId >= trajectoryFrameLength)
            {
                List<Vector3> trajectory = new List<Vector3>(TRAJECTORY_POS_COUNT);
                for (
                    int trajectoryFrameId = frameId - 1;
                    trajectoryFrameId >= frameId - trajectoryFrameLength;
                    trajectoryFrameId -= TRAJECTORY_FRAME_STEP
                ) {
                    var trajectoryPointWorldPos = currClipBuildInfo[trajectoryFrameId].bodyWorldPos;
                    var relativePosition = GetPositionRelativeTo(worldToBody, trajectoryPointWorldPos);
                    trajectory.Add(relativePosition);
                }
                Debug.Assert(trajectory.Count == TRAJECTORY_POS_COUNT);
                currState.pastPositions = trajectory.ToArray();
            }
            
            if (frameId < currClip.Count - trajectoryFrameLength)
            {
                List<Vector3> trajectory = new List<Vector3>(TRAJECTORY_POS_COUNT);
                for (
                    int trajectoryFrameId = frameId + 1;
                    trajectoryFrameId <= frameId + trajectoryFrameLength;
                    trajectoryFrameId += TRAJECTORY_FRAME_STEP
                ) {
                    var trajectoryPointWorldPos = currClipBuildInfo[trajectoryFrameId].bodyWorldPos;
                    var relativePosition = GetPositionRelativeTo(worldToBody, trajectoryPointWorldPos);
                    trajectory.Add(relativePosition);
                }
                Debug.Assert(trajectory.Count == TRAJECTORY_POS_COUNT);
                currState.futurePositions = trajectory.ToArray();
            }

			if (frameId - 1 >= 0)
            {
				var prevState = currClip[frameId - 1].state;

                currState.leftFootVelocity = (currState.leftFootPos - prevState.leftFootPos) / SAMPLE_DELTA_TIME;
                currState.rightFootVelocity = (currState.rightFootPos - prevState.rightFootPos) / SAMPLE_DELTA_TIME;
			}

            // frames that are too close to the start or the end don't get trajectories
            // generated for them.
            if (currState.futurePositions != null && currState.pastPositions != null)
            {
                var frame = currClip[frameId];
                frame.state = currState;
                frames.Add(
                    new FrameId {stateName = stateName, frameNumber = frameId + startFrame},
                    frame
                );
            }
		}
	}

    void FilterCloseMotionStates()
    {
        List<MotionStatePairDistance> motionStateParis = new List<MotionStatePairDistance>();

        var frameArray = frames.ToArray();

        for (int i = 0; i < frameArray.Length; i++)
        {
            var frameA = frameArray[i];
            var databaseDescA = frameA.Value;
            var stateA = databaseDescA.state;
            for (int j = i + 1; j < frameArray.Length; j++)
            {
                var frameB = frameArray[j];
                var stateB = frameB.Value.state;
                motionStateParis.Add(new MotionStatePairDistance {
                    frameIdA = frameA.Key,
                    frameIdB = frameB.Key,
                    distance = MotionStateDistance(stateA, stateB, POSE_VS_TRAJ_BUILD_TIME)
                });
            }
        }
        motionStateParis.Sort((a, b) => a.distance.CompareTo(b.distance));

        int originalCount = motionStateParis.Count;
        int lastDuplicateIndex = (int)(originalCount * DUPLICATE_REMOVAL_RATIO);

        while (lastDuplicateIndex > 0)
        {
            var candidateA = motionStateParis[0].frameIdA;
            var candidateB = motionStateParis[0].frameIdB;

            // Count which of the two appears more often in the closest pairs
            int countA = 0;
            int countB = 0;
            for (int i = 1; i <= lastDuplicateIndex; i++)
            {
                var statePair = motionStateParis[i];
                if (statePair.frameIdA.Equals(candidateA) || statePair.frameIdB.Equals(candidateA))
                {
                    countA++;
                }
                if (statePair.frameIdA.Equals(candidateB) || statePair.frameIdB.Equals(candidateB))
                {
                    countB++;
                }
            }

            FrameId removeTarget = countA > countB ? candidateA : candidateB;

            frames.Remove(removeTarget);

            int removedCount = motionStateParis.RemoveAll(
                x => x.frameIdA.Equals(removeTarget) || x.frameIdB.Equals(removeTarget)
            );
            
            lastDuplicateIndex -= removedCount;
        }
    }

    void CalculateNearestNeighbours()
    {
        List<SortableFrames> frames = new List<SortableFrames>();

        foreach (var frameA in this.frames)
        {
            var databaseDescA = frameA.Value;
            var stateA = databaseDescA.state;
            frames.Clear();
            foreach (var frameB in this.frames)
            {
                var stateB = frameB.Value.state;
                frames.Add(new SortableFrames
                {
                    frameId = frameB.Key,
                    distance = MotionStateDistance(stateA, stateB, POSE_VS_TRAJ_BUILD_TIME)
                });
            }

            frames.Sort((a, b) => a.distance.CompareTo(b.distance));

            databaseDescA.nearestNeighbours = new FrameId[NEAREST_NEIGHBOUR_COUNT];
            for (int i = 0; i < databaseDescA.nearestNeighbours.Length; i++)
            {
                databaseDescA.nearestNeighbours[i] = frames[i].frameId;
            }
        }
    }

    static float PoseDistace(MotionState stateA, MotionState stateB)
    {
        const float POS_WEIGHT = 1;
        const float VEL_WEIGHT = 1.5f;
        //const float TRAJECTORY_WEIGHT = 0.5f;

        // squared distance
        float distanceSqr = 0;

        distanceSqr += POS_WEIGHT * Squared(stateA.hipHeight - stateB.hipHeight);
        distanceSqr += POS_WEIGHT * (stateA.leftFootPos - stateB.leftFootPos).sqrMagnitude;
        distanceSqr += VEL_WEIGHT * (stateA.leftFootVelocity - stateB.leftFootVelocity).sqrMagnitude;
        distanceSqr += POS_WEIGHT * (stateA.rightFootPos - stateB.rightFootPos).sqrMagnitude;
        distanceSqr += VEL_WEIGHT * (stateA.rightFootVelocity - stateB.rightFootVelocity).sqrMagnitude;
        
        return Mathf.Sqrt(distanceSqr);
    }

    static float Squared(float x)
    {
        return x * x;
    }


    static float FutureTrajectoryWeight(int trajectoryPosIndex)
    {
        return 2 * ExponentialDecay(trajectoryPosIndex / (float)TRAJECTORY_POS_COUNT, 1);
    }

    static float PastTrajectoryWeight(int trajectoryPosIndex)
    {
        return ExponentialDecay(trajectoryPosIndex / (float)TRAJECTORY_POS_COUNT, 1);
    }

    static float ExponentialDecay(float t, float lambda)
    {
        return Mathf.Exp(-lambda * t);
    }
    #endregion
}
