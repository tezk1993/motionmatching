﻿/**
 * InputToMotionMatching.cs
 * Created by: #AUTHOR#
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputToMotionMatching : MonoBehaviour
{
    #region Variables
    public float speed = 1;
    MotionMatching motionMatching;
    public bool moving = false;

    #endregion

    #region Unity Methods

    public void Start()
    {
        motionMatching = GetComponent<MotionMatching>();
    }

    public void Update()
    {
        if (Input.GetAxis("Horizontal") > 0.05 || Input.GetAxis("Vertical") > 0.05 || Input.GetAxis("Horizontal") < -0.05 || Input.GetAxis("Vertical") < -0.05)
        {
            motionMatching.currentState = MotionMatching.States.move;


            Vector3 targetVel = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speed;
            motionMatching.targetVelocity = targetVel;
        }
        
        else if(motionMatching.currentState != MotionMatching.States.attack )
        {
            motionMatching.currentState = MotionMatching.States.idle;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            motionMatching.currentState = MotionMatching.States.attack;
        }
        
    }

    #endregion

    #region Methods

    #endregion
}
