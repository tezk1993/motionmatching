﻿/**
 * FollowObject.cs
 * Created by: #AUTHOR#
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    #region Variables
    public Transform target;
    Vector3 offset;
    #endregion

    #region Unity Methods

    public void Start()
    {
        offset = transform.position - target.position;
    }

    public void Update()
    {
        transform.position = target.position + offset;
    }

    #endregion

    #region Methods

    #endregion
}
