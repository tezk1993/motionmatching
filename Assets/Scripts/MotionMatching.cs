﻿/**
 * MotionMatching.cs
 * Created by: Artur Barnabas
 * Created on: 12/10/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

class TrajectoryVisualizer
{
    private List<GameObject> trajectoryPoints;

    public TrajectoryVisualizer(Transform playerTransform, Color color) 
    {
        trajectoryPoints = new List<GameObject>();
        for (int i = 0; i < MotionMatchingDatabase.TRAJECTORY_POS_COUNT * 2; i++)
        {
            var newObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            newObject.transform.parent = playerTransform;
            newObject.transform.localScale = Vector3.one * 0.15f;
            newObject.GetComponent<Renderer>().material.color = color;
            trajectoryPoints.Add(newObject);
        }
    }

    public void UpdatePoints(IEnumerable<Vector3> past, IEnumerable<Vector3> future)
    {
        int index = 0;
        foreach (var point in past)
        {
            trajectoryPoints[index].transform.localPosition = point;
            index++;
        }
        foreach (var point in future)
        {
            trajectoryPoints[index].transform.localPosition = point;
            index++;
        }
    }
}

public class MotionMatching : MonoBehaviour
{
    #region Variables

    [Range(0, 1)]
    public float poseVsTrajectory = 0.7f;

    [HideInInspector]
    public Vector3 targetVelocity;

    // There are two playback animators between which we interpolate to match
    // the requested trajectory.
    Animator[] playbackAnimators;
    public Animator meshAnimator;

    int currPlaybackAnimatorId = 0;
    public Animator currPlaybackAnimator { get { return playbackAnimators[currPlaybackAnimatorId]; } }
    Animator backgroundPlaybackAnimator { get { return playbackAnimators[currPlaybackAnimatorId ^ 1]; } }

    MotionMatchingDatabase database;

    const float TRAJECTORY_SECONDS = MotionMatchingDatabase.TRAJECTORY_SECONDS;

    const int trajectoryUpdateFramerate = 60;
    const float updateDeltaTime = 1 / (float)trajectoryUpdateFramerate;

    const float transitionTime = 0.25f;
    const float newTransitionDelay = 0.5f;

    const float trajectoryCurvature = 0.04f;

    int lastFrameId;

    float currSpeed;

    MotionState currMotionState;

    int lastTrajectoryFrameId;

    FrameId currClosestStateId;

    Vector3[] pastPositionsWorld;
    
    TrajectoryVisualizer playerTrajectoryVisualizer;
    TrajectoryVisualizer candidateTrajectoryVisualizer;

    float transitionStartTime;
    
    HumanBodyBones[] allBones = (HumanBodyBones[])Enum.GetValues(typeof(HumanBodyBones));

    public int idleFrameNumber = 0;

    public int attackFrameNumber = 0;

    public enum States
    {
        move,
        attack,
        idle,
    }

    public States currentState = States.idle;

    public bool moving = false;

    public StepScript anim0Step, anim1step;
    #endregion

    #region Unity Methods

    public void Awake()
    {
        var child = new GameObject();
        child.transform.parent = transform;
        playerTrajectoryVisualizer = new TrajectoryVisualizer(child.transform, Color.white);
        candidateTrajectoryVisualizer = new TrajectoryVisualizer(child.transform, Color.green);

        int pastTrajectorySampleCount = (int)(MotionMatchingDatabase.TRAJECTORY_SECONDS * trajectoryUpdateFramerate);
        pastPositionsWorld = new Vector3[pastTrajectorySampleCount];

        currMotionState = new MotionState();
        currMotionState.futurePositions = new Vector3[MotionMatchingDatabase.TRAJECTORY_POS_COUNT];
        currMotionState.pastPositions = new Vector3[MotionMatchingDatabase.TRAJECTORY_POS_COUNT];

        currClosestStateId = new FrameId { stateName = "mc-circle-cw-walk", frameNumber = 55 };

        //Debug.Log(database.frames.Values);

    }

    public void Start()
    {
        var animators = GetComponentsInChildren<Animator>();
        playbackAnimators = new Animator[animators.Length - 1];
        for (int i = 0; i < playbackAnimators.Length; i++)
        {
            playbackAnimators[i] = animators[i];
        }
        currPlaybackAnimatorId = 0;
        meshAnimator = animators[animators.Length - 1];
        database = UnityEngine.Object.FindObjectOfType<MotionMatchingDatabase>();

        if (database.rebuildDatabase)
        {
            database.RebuildDatabase(currPlaybackAnimator);
        }
        
        // DEBUG
        currPlaybackAnimator.speed = 1;

        // RESET character position and rotation
        currClosestStateId.stateName = database.GetStateNames()[0];
        database.PlayClipFromFrame(currPlaybackAnimator, currClosestStateId);
        var hipTransform = currPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        hipTransform.localPosition = new Vector3(0, hipTransform.position.y, 0);
        hipTransform.localRotation = Quaternion.identity;
        currPlaybackAnimator.Update(1);
        currPlaybackAnimator.applyRootMotion = false;
        currPlaybackAnimator.transform.localPosition = Vector3.zero;
        currPlaybackAnimator.transform.localRotation = Quaternion.identity;
    }

    public void Update()
    {
        if (currentState == States.move)
        {
            if(moving == false)
            {
                currClosestStateId = new FrameId { stateName = "mc-circle-cw-walk", frameNumber = 55 };
                moving = true;
            }
            UpdateMotionState();

            // =====================================================================
            // Find closest motion state from database.

            FindClosestMotionState();

            // =====================================================================

            //////////////////////////////////////////////////////////////////////////////////
            // DEBUG
            FrameId newFrameId = currClosestStateId;
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                newFrameId.frameNumber -= 1;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                newFrameId.frameNumber += 1;
            }
        
            float normalizedTime = database.FrameIdToNormalizedTime(newFrameId);
            if (normalizedTime > 0.7)
            {
                newFrameId.frameNumber = 31;
                normalizedTime = database.FrameIdToNormalizedTime(newFrameId);
            }
            currPlaybackAnimator.applyRootMotion = false;
            //animator.Play(newFrameId.stateName, -1, normalizedTime);
            //currClosestPoseId = newFrameId;
            //////////////////////////////////////////////////////////////////////////////////

            ApplyPoseToMesh();
        }
        /*
        else if (currentState == States.idle)
        {
            float transitionElapsed = Time.time - transitionStartTime;
            if (transitionElapsed > 0.025f)
            {
                moving = false;

                if (idleFrameNumber != database.idleFrameCount)
                {
                    idleFrameNumber++;
                }
                else
                {
                    idleFrameNumber = 0;
                }

                Debug.Log("Play Idle Anim");
                UpdateMotionState();
                //FindClosestMotionState();
                currClosestStateId = new FrameId { stateName = "Idle", frameNumber = idleFrameNumber };
                ExchangeAnimators();
                database.PlayClipFromFrame(currPlaybackAnimator, currClosestStateId);
                transitionStartTime = Time.time;


                ApplyPoseToMesh();
            }
        }
        else if (currentState == States.attack)
        {
            float transitionElapsed = Time.time - transitionStartTime;
            if (transitionElapsed > 0.025f)
            {
                moving = false;

                if(attackFrameNumber != database.attackFrameCount) {
                    attackFrameNumber++;
                }
                else
                {
                    attackFrameNumber = 0;
                    currentState = States.idle;
                }
                //Debug.Log("Play Idle Anim");
                UpdateMotionState();
                //FindClosestMotionState();
                currClosestStateId = new FrameId { stateName = "Attack", frameNumber = attackFrameNumber };
                ExchangeAnimators();
                database.PlayClipFromFrame(currPlaybackAnimator, currClosestStateId);
                transitionStartTime = Time.time;


                ApplyPoseToMesh();
            }
        }
        */
    }

    #endregion

    #region Methods

    void FindClosestMotionState()
    {
        FrameId closest = currClosestStateId;

        float shortestDist = MotionStateDistance(
            database.frames[currClosestStateId].state, currMotionState
        );
        foreach (var neighbourId in database.frames[currClosestStateId].nearestNeighbours)
        {
            var neighbour = database.frames[neighbourId].state;
            float distance = MotionStateDistance(neighbour, currMotionState);
            if (distance < shortestDist)
            {
                closest = neighbourId;
                shortestDist = distance;
            }
        }
        currClosestStateId = closest;
        float transitionElapsed = Time.time - transitionStartTime;
        if (transitionElapsed > newTransitionDelay)
        {
            var closestDesc = database.frames[currClosestStateId].state;
            Debug.Assert(closestDesc.pastPositions.Length == MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
            candidateTrajectoryVisualizer.UpdatePoints(
                closestDesc.pastPositions,
                closestDesc.futurePositions
            );
            // Start playing that clip from the next frame after the closest pose and start
            var nextFrameId = currClosestStateId;
            nextFrameId.frameNumber += 1;
            ExchangeAnimators();
            database.PlayClipFromFrame(currPlaybackAnimator, nextFrameId);
            transitionStartTime = Time.time;
        }
    }

    float MotionStateDistance(MotionState stateA, MotionState stateB)
    {
        return MotionMatchingDatabase.MotionStateDistance(stateA, stateB, poseVsTrajectory);
    }

    void ExchangeAnimators()
    {
        var currStateInfo = currPlaybackAnimator.GetCurrentAnimatorStateInfo(0);
        var currClipNameHash = currStateInfo.fullPathHash;
        var currClipNormalizedTime = currStateInfo.normalizedTime;

        backgroundPlaybackAnimator.Play(currClipNameHash, -1, currClipNormalizedTime);

        currPlaybackAnimatorId ^= 1; // swap animators
    }

    void UpdateMotionState()
    {
        // =====================================================================
        // Update trajectories
        Vector3 velocity = currSpeed * transform.forward;
        Vector3 prevSegmentPos = transform.position;
        prevSegmentPos.y = 0;
        List<Vector3> futurePositions = new List<Vector3>();
        for (float elapsedTime = 0; elapsedTime < TRAJECTORY_SECONDS; elapsedTime += updateDeltaTime)
        {
            velocity = Vector3.Lerp(velocity, targetVelocity, trajectoryCurvature);
            var segmentPos = prevSegmentPos + velocity * updateDeltaTime;
            futurePositions.Add(segmentPos);
            prevSegmentPos = segmentPos;
        }
        var worldToBody = MotionMatchingDatabase.GetWorldToBodyTransform(meshAnimator);
        int stepSize = (int)((trajectoryUpdateFramerate * TRAJECTORY_SECONDS) / MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
        for (int i = 0; i < MotionMatchingDatabase.TRAJECTORY_POS_COUNT; i++)
        {
            var position = futurePositions[i * stepSize];
            currMotionState.futurePositions[i] = worldToBody.MultiplyPoint(position);
        }

        // Update past trajectory
        var hipPos = meshAnimator.GetBoneTransform(HumanBodyBones.Hips).position;
        int currTrajectoryFrameId = (int)(Time.time * trajectoryUpdateFramerate);
        int diff = currTrajectoryFrameId - lastTrajectoryFrameId;
        lastTrajectoryFrameId = currTrajectoryFrameId;
        Vector3 currBodyPosition = MotionMatchingDatabase.GetBodyPositionFromHip(hipPos);
        if (diff > 0)
        {
            for (int i = pastPositionsWorld.Length-1; i >= diff; i--)
            {
                pastPositionsWorld[i] = pastPositionsWorld[i-diff];
            }
            for (int i = 0; i < diff; i++)
            {
                pastPositionsWorld[i] = currBodyPosition;
            }
        }
        stepSize = (int)((trajectoryUpdateFramerate * TRAJECTORY_SECONDS) / MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
        for (int i = 0; i < currMotionState.pastPositions.Length; i++)
        {
            var position = pastPositionsWorld[i * stepSize];
            currMotionState.pastPositions[i] = worldToBody.MultiplyPoint(position);
        }

        playerTrajectoryVisualizer.UpdatePoints(
            currMotionState.pastPositions,
            currMotionState.futurePositions
        );
        // =====================================================================

        // =====================================================================
        // Update limb state
        worldToBody = MotionMatchingDatabase.GetWorldToBodyTransform(meshAnimator);
        var leftFootPos = meshAnimator.GetBoneTransform(HumanBodyBones.LeftFoot).position;
        anim0Step.leftFootpos = leftFootPos;
        anim1step.leftFootpos = leftFootPos;
        leftFootPos = worldToBody.MultiplyPoint(leftFootPos);
        var rightFootPos = meshAnimator.GetBoneTransform(HumanBodyBones.RightFoot).position;
        anim0Step.rightFootpos = rightFootPos;
        anim1step.rightFootpos = rightFootPos;
        rightFootPos = worldToBody.MultiplyPoint(rightFootPos);



        var prevLeftFootPos = currMotionState.leftFootPos;
        var prevRightFootPos = currMotionState.rightFootPos;

        currMotionState.hipHeight = hipPos.y;
        currMotionState.leftFootVelocity = (leftFootPos - prevLeftFootPos) / Time.deltaTime;
        currMotionState.rightFootVelocity = (rightFootPos - prevRightFootPos) / Time.deltaTime;
        currMotionState.leftFootPos = leftFootPos;
        currMotionState.rightFootPos = rightFootPos;
        // =====================================================================
    }

    void GetTrajectoryDelta(out Vector3 deltaPos, out Quaternion deltaRot)
    {
        Vector3 velocity = currSpeed * transform.forward;
        int frameId = (int)(Time.time * trajectoryUpdateFramerate);
        int frameDiff = frameId - lastFrameId;
        lastFrameId = frameId;
        if (frameDiff > 0)
        {
            Vector3 appliedVel = Vector3.Lerp(velocity, targetVelocity, trajectoryCurvature);
            currSpeed = appliedVel.magnitude;
            if (currSpeed > 0.05)
            {
                deltaPos = appliedVel * Time.deltaTime;

                var newRot = Quaternion.LookRotation(appliedVel.normalized, Vector3.up);
                deltaRot = newRot * Quaternion.Inverse(transform.rotation);
                return;
            }
        }

        deltaPos = Vector3.zero;
        deltaRot = Quaternion.identity;
    }

    void ApplyPoseToMesh()
    {
        var transitionRatio = Mathf.Min(1, (Time.time - transitionStartTime) / transitionTime);

        for (int boneIndex = 0; boneIndex < (int)HumanBodyBones.LastBone; boneIndex++)
        {
            var boneId = (HumanBodyBones)boneIndex;
            var bgTransform = backgroundPlaybackAnimator.GetBoneTransform(boneId);
            var targetTransform = currPlaybackAnimator.GetBoneTransform(boneId);
            if (bgTransform != null)
            {
                var meshTransform = meshAnimator.GetBoneTransform(boneId);
                meshTransform.localRotation = Quaternion.Slerp(
                    bgTransform.localRotation, targetTransform.localRotation, transitionRatio
                );
            }
		}
        var bgHipTransform = backgroundPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        var targetHipTransform = currPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        var meshHipTransform = meshAnimator.GetBoneTransform(HumanBodyBones.Hips);
        meshHipTransform.localPosition = Vector3.Lerp(
            bgHipTransform.localPosition, targetHipTransform.localPosition, transitionRatio
        );

        // Interpolate between requested trajectory and playback movement.
        var animationDeltaPos = Vector3.Lerp(
            backgroundPlaybackAnimator.deltaPosition, currPlaybackAnimator.deltaPosition, transitionRatio
        );
        animationDeltaPos.Scale(new Vector3(1, 0, 1));
        var animationDeltaRot = Quaternion.Slerp(
            backgroundPlaybackAnimator.deltaRotation, currPlaybackAnimator.deltaRotation, transitionRatio
        );

        Vector3 inputDeltaPos;
        Quaternion inputDeltaRot;
        GetTrajectoryDelta(out inputDeltaPos, out inputDeltaRot);
        if(moving == true) { 
            const float forceInput = 0.25f;
            transform.position += Vector3.Lerp(animationDeltaPos, inputDeltaPos, forceInput);
            transform.rotation *= Quaternion.Slerp(animationDeltaRot, inputDeltaRot, forceInput * 0.5f);
        }
    }

    #endregion
}
